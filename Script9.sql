-- OBS: Como o comando a seguir é para controle de acesso, não há necessidade de usar o comando inicial USER para chamar o banco de dados, pois
-- estes comandos são aplicados a nível de servidor: --

-- Comando para CRIAR usuário: --
-- Neste caso o usuário só conseguirá conectar ao banco de dados se tiver com o NOME, a SENHA e o IP especificados corretos. --
/* CREATE USER 'andre'@'200.200.190.190' IDENTIFIED BY 'milani123456'; */

-- Neste caso o usuário poderá se conectar ao banco de dados a partir de QUALQUER endereço de IP. --
/* CREATE USER 'andre'@'%' IDENTIFIED BY 'milani123456'; */

-- Neste caso o usuário só conseguirá conectar ao banco de dados se tiver na PRÓPRIA MÁQUINA de trabalho do usuário. -- 
CREATE USER 'andre'@'localhost' IDENTIFIED BY 'milani123456';

-- Comando para CONCEDER ACESSO ao usuário André, ou seja, definir um administrador do banco de dados: --
-- Para conceder acesso a todos os níveis (uso do termo ALL): --
GRANT ALL ON curso_sql.* TO 'andre'@'localhost';

-- Para conceder acesso apenas para leitura dos dados quando o usuário André precisar viajar e quiser manter a segurança do banco de dados: --
CREATE USER 'andre'@'%' IDENTIFIED BY 'andreviagem';
GRANT SELECT ON curso_sql.* TO 'andre'@'%';
-- Para conceder acesso ao comando INSERT na conta do usuário André: --
/* GRANT INSERT ON curso_sql.* TO 'andre'@'%'; */

-- Para conceder acesso ao comando INSERT na conta de viagem do usuário André, porém apenas na tabela funcionários: --
GRANT INSERT ON curso_sql.funcionarios TO 'andre'@'%';

-- Comandos para REMOVER os acessos criados: --
-- Comando para remover da conta do usuário André o acesso a INSERÇÃO de dados na tabela funcionários: --
REVOKE INSERT ON curso_sql.funcionarios FROM 'andre'@'%'; 
-- Comando para remover da conta do usuário André o acesso a SELEÇÃO de dados na tabela salários: --
REVOKE SELECT ON curso_sql.* FROM 'andre'@'%';

-- OBS: O comando de REVOKE remove apensas o o que especificado antes, ou seja, se for concedido a tudo (*), então deve ser removido tudo!! --
-- Logo, para conceder acesso de forma específica, devemos fazer: --
GRANT SELECT ON curso_sql.funcionarios TO 'andre'@'%';
GRANT SELECT ON curso_sql.veiculos TO 'andre'@'%';

-- Agora, para remover de forma específica, faremos: --
REVOKE SELECT ON curso_sql.funcionarios FROM 'andre'@'%';
REVOKE SELECT ON curso_sql.veiculos FROM 'andre'@'%';

-- Comando para REMOVER todo o acesso do usuário André do endereço Localhost: --
REVOKE ALL ON curso_sql.* FROM 'andre'@'localhost';

-- Comando para DELETAR os usuários André: --
DROP USER 'andre'@'%';
DROP USER 'andre'@'localhost';

-- Comando para CONSULTAR quais usuários estão criados e quais acessos eles possuem: --
SELECT USER FROM mysql.USER;
SHOW GRANTS FOR 'andre'@'%';
