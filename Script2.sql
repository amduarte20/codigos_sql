-- Código do Exercício Proposto 1 da Aula 3 --

CREATE DATABASE SOFTBLUE DEFAULT CHARSET=latin1;
USE SOFTBLUE;

CREATE TABLE TIPO 
(
	CODIGO integer unsigned not null auto_increment,
    TIPO varchar(32) not null,
    PRIMARY KEY (CODIGO)    
);

CREATE TABLE INSTRUTOR 
(
	CODIGO integer unsigned not null auto_increment,
    INSTRUTOR varchar(64) not null,
    TELEFONE varchar(9) null,
    PRIMARY KEY (CODIGO)
);

CREATE TABLE CURSO 
(
	CODIGO integer unsigned not null auto_increment,
    CURSO varchar(64) not null,
    TIPO integer unsigned not null,
    INSTRUTOR integer unsigned not null,
    VALOR double not null,
    PRIMARY KEY (CODIGO),
    INDEX FK_TIPO(TIPO),
    INDEX FK_INSTRUTOR(INSTRUTOR),
    FOREIGN KEY(TIPO) REFERENCES TIPO(CODIGO),
    FOREIGN KEY(INSTRUTOR) REFERENCES INSTRUTOR(CODIGO)
);

CREATE TABLE ALUNO 
(
	CODIGO integer unsigned not null auto_increment,
    ALUNO varchar(64) not null,
    ENDERECO varchar(230) not null,
    EMAIL varchar(128) not null,
    PRIMARY KEY (CODIGO)
);

CREATE TABLE PEDIDO 
(
	CODIGO integer unsigned not null auto_increment,
    ALUNO integer unsigned not null,
    DATAHORA datetime not null,
    PRIMARY KEY (CODIGO),
    INDEX FK_ALUNO(ALUNO),
    FOREIGN KEY(ALUNO) REFERENCES ALUNO(CODIGO)
    );

CREATE TABLE PEDIDO_DETALHE 
(
	PEDIDO integer unsigned not null,
    CURSO integer unsigned not null,
    VALOR double not null,
    INDEX FK_PEDIDO(PEDIDO),
    INDEX FK_CURSO(CURSO),
    PRIMARY KEY(PEDIDO, CURSO),
    FOREIGN KEY(PEDIDO) REFERENCES PEDIDO(CODIGO),
    FOREIGN KEY(CURSO) REFERENCES CURSO(CODIGO)
);

-- Código do Exercício Proposto 2 da Aula 3 --

ALTER TABLE ALUNO ADD DATA_NASCIMENTO varchar(10);

ALTER TABLE ALUNO CHANGE COLUMN DATA_NASCIMENTO NASCIMENTO DATE null;

ALTER TABLE ALUNO ADD INDEX INDEX_ALUNO(ALUNO);

ALTER TABLE INSTRUTOR ADD EMAIL varchar(100);

ALTER TABLE CURSO ADD INDEX INDEX_INSTRUTOR(INSTRUTOR);

ALTER TABLE INSTRUTOR DROP EMAIL;

-- Código do Exercício Proposto 1 da Aula 4:--
USE SOFTBLUE;
-- Comando para adicionar registros na tabela TIPO: --
INSERT INTO tipo (CODIGO, TIPO) VALUES (1, 'Banco de Dados');
INSERT INTO tipo (CODIGO, TIPO) VALUES (2, 'Programação');
INSERT INTO tipo (CODIGO, TIPO) VALUES (3, 'Modelagem de Dados');
SELECT * FROM tipo;

-- Comando para adicionar registros na tabela INSTRUTOR: --
INSERT INTO instrutor (CODIGO, INSTRUTOR, TELEFONE) VALUES (1, 'André Milani', '1111-1111');
INSERT INTO instrutor (CODIGO, INSTRUTOR, TELEFONE) VALUES (2, 'Carlos Tosin', '1212-1212');
SELECT * FROM instrutor;

-- Comando para adicionar registros na tabela CURSO: --
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (1, 'Java Fundamentos', 2, 2, 270);
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (2, 'Java Avançado', 2, 2, 330);
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (3, 'SQL Completo', 1, 1, 170);
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (4, 'Php Básico', 2, 1, 270);
SELECT * FROM curso;

-- Comando para adicionar registros na tabela ALUNO: --
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (1, 'José', 'Rua XV de Novembro 72', 'jose@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (2, 'Wagner', 'Av. Paulista', 'wagner@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (3, 'Emílio', 'Rua Lajes 103, ap: 701', 'emilio@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (4, 'Cris', 'Rua Tauney 22', 'cris@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (5, 'Regina', 'Rua Salles 305', 'regina@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (6, 'Fernando', 'Av. Central 30', 'fernando@softblue.com.br');
SELECT * FROM aluno;

-- Comando para adicionar registros na tabela PEDIDO: --
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (1, 2, '2010-04-15 11:23:32');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (2, 2, '2010-04-15 14:36:21');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (3, 3, '2010-04-16 11:17:45');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (4, 4, '2010-04-17 14:27:22');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (5, 5, '2010-04-18 11:18:19');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (6, 6, '2010-04-19 13:47:35');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (7, 6, '2010-04-20 18:13:44');
SELECT * FROM pedido;

-- Comando para adicionar registros na tabela PEDIDO_DETALHE: --
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (1, 1, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (1, 2, 330);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (2, 1, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (2, 2, 330);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (2, 3, 170);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (3, 4, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (4, 2, 330);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (4, 4, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (5, 3, 170);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (6, 3, 170);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (7, 4, 270);
SELECT * FROM pedido_detalhe;

-- Código do Exercício Proposto 2 da Aula 4:--

-- Exibir todas as informações de todos os alunos: --
SELECT * FROM aluno;

-- Exibir somente o título de cada curso da Softblue: --
SELECT CURSO FROM curso;

-- Exibir somente o título e valor de cada curso da Softblue cujo preço seja maior que 200: -- 
SELECT CURSO, VALOR FROM curso WHERE VALOR > 200;

-- Exibir somente o título e valor de cada curso da Softblue cujo preço seja maior que 200 e menor que 300: --
SELECT CURSO, VALOR FROM curso WHERE VALOR > 200 AND VALOR < 300;
-- Outra solução para o exercício seria esta (usando o comando Between): --
SELECT CURSO, VALOR FROM curso WHERE VALOR BETWEEN 200 AND 300;

-- Exibir as informações da tabela PEDIDOS para os pedidos realizados entre 15/04/2010 e 18/04/2010: --
SELECT DATAHORA FROM pedido WHERE DATAHORA > '2010-04-15' AND DATAHORA < '2010-04-19';
-- Outra solução para o exercício seria esta (usando o comando Between): --
SELECT DATAHORA FROM pedido WHERE DATAHORA BETWEEN '2010-04-15' AND '2010-04-19';

-- Exibir as informações da tabela PEDIDOS para os pedidos realizados na data de 15/04/2010: --
SELECT DATAHORA FROM pedido WHERE DATE(DATAHORA) = '2010-04-15';

-- Código do Exercício Proposto 3 da Aula 4:--

-- Altere o endereço do aluno JOSÉ para 'Av. Brasil 778': --
UPDATE ALUNO SET ENDERECO = 'Av. Brasil 778' WHERE CODIGO = 1;
SELECT * FROM aluno;

-- Altere o e-mail do aluno CRIS para 'cristiano@softblue.com.br': --
UPDATE ALUNO SET EMAIL = 'cristiano@softblue.com.br' WHERE CODIGO = 4;
SELECT * FROM aluno;

-- Aumente em 10% o valor dos cursos abaixo de 300: --
SELECT * FROM curso;
UPDATE CURSO SET VALOR = ROUND(VALOR * 1.1, 1) WHERE VALOR < 300;
SELECT * FROM curso;

-- Altere o nome do curso de Php Básico para Php Fundamentos: --
UPDATE CURSO SET CURSO = 'Php Fundamentos' WHERE CURSO = 'Php Básico';
SELECT * FROM curso;
