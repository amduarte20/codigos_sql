-- Comando para chamar o Banco de Dados: --
USE curso_sql;

-- Comando que mostra os mecanismos de armazenamento que o SQL oferece ao usuário: --
SHOW ENGINES;

-- Comando para criar uma tabela com suporte para transações (InnoDB): --
CREATE TABLE contas_bancarias 
(
	id int unsigned not null auto_increment,
    titular varchar(45) not null,
    saldo double not null,
    PRIMARY KEY (id)
) engine = InnoDB;

-- Comando para inserir alguns registros na tabela: --
INSERT INTO contas_bancarias (titular, saldo) VALUES ('André', 1000);
INSERT INTO contas_bancarias (titular, saldo) VALUES ('Carlos', 2000);
SELECT * FROM contas_bancarias;

-- Comando de Ação de Tranferência de 100 Reais da conta do André para a conta do Carlos: --

-- Quando a transação for ANULADA: --
start transaction; -- Comando para iniciar a transação: --
UPDATE contas_bancarias SET saldo = saldo - 100 WHERE id = 1; -- Comando para retirar o valor da conta de André: --
UPDATE contas_bancarias SET saldo = saldo + 100 WHERE id = 2; -- Comando para colocar o valor na conta do Carlos: --
rollback; -- Comando para ANULAR a transação: --

-- Quando a transação for CONFIRMADA: --
start transaction; -- Comando para iniciar a transação: --
UPDATE contas_bancarias SET saldo = saldo - 100 WHERE id = 1; -- Comando para retirar o valor da conta de André: --
UPDATE contas_bancarias SET saldo = saldo + 100 WHERE id = 2; -- Comando para colocar o valor na conta do Carlos: --
commit; -- Comando para CONFIRMAR a transação: --
 