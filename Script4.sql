USE curso_sql;

SELECT * FROM funcionarios;
SELECT * FROM veiculos;

-- Comando para criar um relacionamento usando a ferramente INNER JOIN: --
SELECT * FROM funcionarios INNER JOIN veiculos ON funcionario_id = funcionarios.id;
-- Ou podemos também usar assim: --
SELECT * FROM funcionarios INNER JOIN veiculos ON veiculos.funcionario_id = funcionarios.id;
-- Ou também podemos usar com os apelidos: --
SELECT * FROM funcionarios f INNER JOIN veiculos v ON v.funcionario_id = f.id;

-- Comandos para criar um relacionamento usando LEFT JOIN: --
SELECT * FROM funcionarios f LEFT JOIN veiculos v ON v.funcionario_id = f.id;
-- Comandos para criar um relacionamento usando RIGHT JOIN: --
SELECT * FROM funcionarios f RIGHT JOIN veiculos v ON v.funcionario_id = f.id;
-- Adicionei um registro na tabela para poder ver o efeito do comando RIGHT JOIN: --
INSERT INTO veiculos (funcionario_id, veiculo, placa) VALUES (null, "Moto", "SB-0003");
SELECT * FROM funcionarios f RIGHT JOIN veiculos v ON v.funcionario_id = f.id;

-- Comando para criar um relacionamento usando FULL JOIN: --
SELECT * FROM funcionarios f FULL JOIN veiculos v ON v.funcionario_id = f.id;
-- Quando o MySQL não tiver o comando FULL JOIN, usamos o comando UNION junto ao LEFT e RIGHT JOIN para obter o mesmo efeito: --
SELECT * FROM funcionarios f LEFT JOIN veiculos v ON v.funcionario_id = f.id
UNION
SELECT * FROM funcionarios f RIGHT JOIN veiculos v ON v.funcionario_id = f.id;

-- Comando para criar um relacionamento usando EQUI JOIN: --
CREATE TABLE cpfs 
(
id int unsigned not null,
cpf varchar(14) not null,
PRIMARY KEY (id),
CONSTRAINT fk_cpf FOREIGN KEY (id) REFERENCES funcionarios (id)
);

INSERT INTO cpfs (id, cpf) VALUES (1, '111.111.111-11');
INSERT INTO cpfs (id, cpf) VALUES (2, '222.222.222-22');
INSERT INTO cpfs (id, cpf) VALUES (3, '333.333.333-33');
INSERT INTO cpfs (id, cpf) VALUES (5, '555.555.555-55');
SELECT * FROM cpfs;

SELECT * FROM funcionarios INNER JOIN cpfs ON funcionarios.id = cpfs.id;
-- Ou também pelo comando: --
SELECT * FROM funcionarios INNER JOIN cpfs USING (id);

-- Comando para criar um relacionamento usando SELF JOIN: --

-- Criando primeiramente uma tabela nova e adicionando registros nesta: --
CREATE TABLE clientes 
(
	id int unsigned not null auto_increment,
    nome varchar(45) not null,
    quem_indicou int unsigned,
    PRIMARY KEY (id),
    CONSTRAINT fk_quem_indicou FOREIGN KEY (quem_indicou) REFERENCES clientes (id)
);
INSERT INTO clientes (id, nome, quem_indicou) VALUES (1,'André', NULL);
INSERT INTO clientes (id, nome, quem_indicou) VALUES (2,'Samuel', 1);
INSERT INTO clientes (id, nome, quem_indicou) VALUES (3,'Carlos', 2);
INSERT INTO clientes (id, nome, quem_indicou) VALUES (4,'Rafael', 1);
SELECT * FROM clientes;
-- Comando para criar um relacionamento usando SELF JOIN: --
SELECT a.nome AS CLIENTE, b.nome AS "QUEM INDICOU" FROM clientes a JOIN clientes b ON a.quem_indicou = b.id;

-- Comando para criar um relacionamento TRIPLO: --
SELECT * FROM funcionarios INNER JOIN veiculos ON veiculos.funcionario_id = funcionarios.id INNER JOIN cpfs ON cpfs.id = funcionarios.id;
-- Usando o mesmo comando, porém usando os apelidos: --
SELECT * FROM funcionarios f INNER JOIN veiculos  v ON v.funcionario_id = f.id INNER JOIN cpfs ON cpfs.id = f.id;

-- Aprendendo comandos de VIEW (VISÕES): --

-- VISÕES: Tratam-se de comandos dados para consultas que ficam armazenadas no Banco de Dados. --

CREATE VIEW funcionarios_a AS SELECT * FROM  funcionarios WHERE salario >= 1700;
SELECT * FROM funcionarios_a;
UPDATE funcionarios SET salario = 1500 WHERE id = 3;
DROP VIEW funcionarios_a;

CREATE VIEW funcionarios_a AS SELECT * FROM  funcionarios WHERE salario >= 2000;
SELECT * FROM funcionarios_a;
DROP VIEW funcionarios_a;

CREATE VIEW funcionarios_a AS SELECT * FROM  funcionarios WHERE salario >= 1500;
SELECT * FROM funcionarios_a;
