USE curso_sql;
-- Comandos para adicionar registros na tabela: --
INSERT INTO funcionarios (id, nome, salario, departamento) VALUES (1, 'Fernando', 1400, 'TI');
INSERT INTO funcionarios (id, nome, salario, departamento) VALUES (2, 'Guilherme', 2500, 'Jurídico');
INSERT INTO funcionarios (nome, salario, departamento) VALUES ('Fábio', 1700, 'TI');
INSERT INTO funcionarios (nome, salario, departamento) VALUES ('José', 1800, 'Marketing');
INSERT INTO funcionarios (nome, salario, departamento) VALUES ('Isabela', 2200, 'Jurídico');

-- Comandos para consulta de tabela ou resgistros expecíficos dentro dela: --
SELECT * FROM funcionarios;
SELECT * FROM funcionarios WHERE salario > 2000;
SELECT * FROM funcionarios WHERE nome = 'José';
SELECT * FROM funcionarios WHERE id = 3;

-- Tentativa de aumanetar em 10% o salário de todos na tabela (o comando abaixo da erro)
UPDATE funcionarios SET salario = salario * 1.1; 
-- Tentativa de aumentar em 10% o salário só do Fernando (o comando abaixo funciona)
UPDATE funcionarios SET salario = salario * 1.1 WHERE id = 1;
-- Tentativa de aumanetar em 10% o salário de todos na tabela (o comando abaixo funciona)
SET SQL_SAFE_UPDATES = 0; -- Este comando deve ser usado para executar o comando desejado na tabela inteira sem precisar fazer 1 a 1
/*SET SQL_SAFE_UPADTAES = 1*/ -- Este camondo serve para habilitar novamente o comando UPDATE com exigência do WHERE
UPDATE funcionarios SET salario = ROUND(salario * 1.1, 2); -- O comando ROUND serve para arredondar as casas decimais das operações.

-- Comando para excluir funcionários:
DELETE FROM funcionarios WHERE id = 4;

-- Comando para adicionar alguns veículos na tabela de interesse:
INSERT INTO veiculos (funcionario_id, veiculo, placa) VALUES (1, 'Carro', 'SB-0001');
INSERT INTO veiculos (funcionario_id, veiculo, placa) VALUES (1, 'Carro', 'SB-0002');
SELECT * FROM veiculos;

-- Passando o veiculo 'SB 0002' para a funcionaria Isabela:
UPDATE veiculos SET funcionario_id = 5 WHERE id = 2;

-- Adicionando informaçõe na tabela salarios:
INSERT INTO salarios (faixa, inicio, fim) VALUES ('Analista Jr.', 1000, 2000);
INSERT INTO salarios (faixa, inicio, fim) VALUES ('Analista Pleno', 2000, 4000);
SELECT * FROM salarios;

-- Comando para inserir um apelido para a tabela de interesse: OBS: O comando de apelido geralmente é usado no tópico relacionamento
SELECT * FROM funcionarios f WHERE f.salario > 2000;

-- Comando para mostrar numa tabela o nome e o salario dos funcionarios de interesse:
SELECT nome, salario FROM funcionarios f WHERE f.salario > 2000;

-- Comando para colocar apelido no campo de interesse da tabela desejada:
SELECT nome AS 'Funcionário', salario FROM funcionarios f WHERE f.salario > 2000;
SELECT nome AS 'Funcionário', salario AS 'Salário' FROM funcionarios f WHERE f.salario > 2000;

-- Comando para utilizar a função UNION para unir/juntar duas pesquisas:
SELECT * FROM funcionarios WHERE nome = 'Guilherme'
UNION
SELECT * FROM funcionarios WHERE id = 5;

-- OBS: O comando abaixo mostra a diferença entre o funcionamento do atributo UNION e do UNION ALL:
SELECT * FROM funcionarios WHERE nome = 'Guilherme'
UNION
SELECT * FROM funcionarios WHERE nome = 'Guilherme';
-- Aplicando agora o UNION ALL:
SELECT * FROM funcionarios WHERE nome = 'Guilherme'
UNION ALL
SELECT * FROM funcionarios WHERE nome = 'Guilherme';