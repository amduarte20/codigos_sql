-- Comando para chamar o Banco de Dados: --
USE curso_sql;

-- Comando para criar uma Tabela de Exemplo que será usada em Stored Procedures e Triggers: --

CREATE TABLE pedidos 
(
	id int unsigned not null auto_increment,
    descricao varchar(100) not null,
    valor double not null default '0',
    pago varchar(3) not null default 'Não',
    PRIMARY KEY(id)
);

-- Comando para inserir alguns registros na tabela: --
INSERT INTO pedidos (descricao, valor) VALUES ('TV', 3000);
INSERT INTO pedidos (descricao, valor) VALUES ('Geladeira', 1400);
INSERT INTO pedidos (descricao, valor) VALUES ('DVD Player', 300);
SELECT * FROM pedidos;

-- Código para fazer uma chamada de comando armazenado: --
CALL limpa_pedidos();
SELECT * FROM pedidos;

-- Comandos para construir as Triggers (Gatilhos): --

-- Comando para limpar automaticamente a tabela de pedidos, toda vez que a tabela estoque receber um novo produto: --
CREATE TABLE estoque 
(
	id int unsigned not null auto_increment,
    descricao varchar(50) not null,
    quantidade int not null,
    PRIMARY KEY(id)
);

-- Comando para criar um Trigger: --
CREATE TRIGGER gatilho_limpa_pedidos
BEFORE INSERT ON estoque
FOR EACH ROW
CALL limpa_pedidos();
SELECT * FROM pedidos;

-- Comando para inserir alguns registros na tabela: --
INSERT INTO pedidos (descricao, valor) VALUES ('TV', 3000);
INSERT INTO pedidos (descricao, valor) VALUES ('Geladeira', 1400);
INSERT INTO pedidos (descricao, valor) VALUES ('DVD Player', 300);
SELECT * FROM pedidos;

-- Comando para inserir um novo registro: --
INSERT INTO estoque (descricao, quantidade) VALUES ('Fogão', 5);
SELECT * FROM pedidos;
SELECT * FROM estoque;

-- Comando para alterar a coluna pago do produto Geladeira: --
UPDATE pedidos SET pago = 'Sim' WHERE id = 8;
SELECT * FROM pedidos;

-- Comando para inserir um novo registro: --
INSERT INTO estoque (descricao, quantidade) VALUES ('Forno', 3);
SELECT * FROM pedidos;
SELECT * FROM estoque;




