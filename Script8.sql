-- Código do Exercício Proposto 1 da Aula 3 --

CREATE DATABASE SOFTBLUE DEFAULT CHARSET=latin1;
USE SOFTBLUE;

CREATE TABLE TIPO 
(
	CODIGO integer unsigned not null auto_increment,
    TIPO varchar(32) not null,
    PRIMARY KEY (CODIGO)    
);

CREATE TABLE INSTRUTOR 
(
	CODIGO integer unsigned not null auto_increment,
    INSTRUTOR varchar(64) not null,
    TELEFONE varchar(9) null,
    PRIMARY KEY (CODIGO)
);

CREATE TABLE CURSO 
(
	CODIGO integer unsigned not null auto_increment,
    CURSO varchar(64) not null,
    TIPO integer unsigned not null,
    INSTRUTOR integer unsigned not null,
    VALOR double not null,
    PRIMARY KEY (CODIGO),
    INDEX FK_TIPO(TIPO),
    INDEX FK_INSTRUTOR(INSTRUTOR),
    FOREIGN KEY(TIPO) REFERENCES TIPO(CODIGO),
    FOREIGN KEY(INSTRUTOR) REFERENCES INSTRUTOR(CODIGO)
);

CREATE TABLE ALUNO 
(
	CODIGO integer unsigned not null auto_increment,
    ALUNO varchar(64) not null,
    ENDERECO varchar(230) not null,
    EMAIL varchar(128) not null,
    PRIMARY KEY (CODIGO)
);

CREATE TABLE PEDIDO 
(
	CODIGO integer unsigned not null auto_increment,
    ALUNO integer unsigned not null,
    DATAHORA datetime not null,
    PRIMARY KEY (CODIGO),
    INDEX FK_ALUNO(ALUNO),
    FOREIGN KEY(ALUNO) REFERENCES ALUNO(CODIGO)
    );

CREATE TABLE PEDIDO_DETALHE 
(
	PEDIDO integer unsigned not null,
    CURSO integer unsigned not null,
    VALOR double not null,
    INDEX FK_PEDIDO(PEDIDO),
    INDEX FK_CURSO(CURSO),
    PRIMARY KEY(PEDIDO, CURSO),
    FOREIGN KEY(PEDIDO) REFERENCES PEDIDO(CODIGO),
    FOREIGN KEY(CURSO) REFERENCES CURSO(CODIGO)
);

-- Código do Exercício Proposto 2 da Aula 3 --

ALTER TABLE ALUNO ADD DATA_NASCIMENTO varchar(10);

ALTER TABLE ALUNO CHANGE COLUMN DATA_NASCIMENTO NASCIMENTO DATE null;

ALTER TABLE ALUNO ADD INDEX INDEX_ALUNO(ALUNO);

ALTER TABLE INSTRUTOR ADD EMAIL varchar(100);

ALTER TABLE CURSO ADD INDEX INDEX_INSTRUTOR(INSTRUTOR);

ALTER TABLE INSTRUTOR DROP EMAIL;

-- Código do Exercício Proposto 1 da Aula 4:--
USE SOFTBLUE;
-- Comando para adicionar registros na tabela TIPO: --
INSERT INTO tipo (CODIGO, TIPO) VALUES (1, 'Banco de Dados');
INSERT INTO tipo (CODIGO, TIPO) VALUES (2, 'Programação');
INSERT INTO tipo (CODIGO, TIPO) VALUES (3, 'Modelagem de Dados');
SELECT * FROM tipo;

-- Comando para adicionar registros na tabela INSTRUTOR: --
INSERT INTO instrutor (CODIGO, INSTRUTOR, TELEFONE) VALUES (1, 'André Milani', '1111-1111');
INSERT INTO instrutor (CODIGO, INSTRUTOR, TELEFONE) VALUES (2, 'Carlos Tosin', '1212-1212');
SELECT * FROM instrutor;

-- Comando para adicionar registros na tabela CURSO: --
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (1, 'Java Fundamentos', 2, 2, 270);
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (2, 'Java Avançado', 2, 2, 330);
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (3, 'SQL Completo', 1, 1, 170);
INSERT INTO curso (CODIGO, CURSO, TIPO, INSTRUTOR, VALOR) VALUES (4, 'Php Básico', 2, 1, 270);
SELECT * FROM curso;

-- Comando para adicionar registros na tabela ALUNO: --
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (1, 'José', 'Rua XV de Novembro 72', 'jose@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (2, 'Wagner', 'Av. Paulista', 'wagner@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (3, 'Emílio', 'Rua Lajes 103, ap: 701', 'emilio@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (4, 'Cris', 'Rua Tauney 22', 'cris@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (5, 'Regina', 'Rua Salles 305', 'regina@softblue.com.br');
INSERT INTO aluno (CODIGO, ALUNO, ENDERECO, EMAIL) VALUES (6, 'Fernando', 'Av. Central 30', 'fernando@softblue.com.br');
SELECT * FROM aluno;

-- Comando para adicionar registros na tabela PEDIDO: --
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (1, 2, '2010-04-15 11:23:32');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (2, 2, '2010-04-15 14:36:21');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (3, 3, '2010-04-16 11:17:45');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (4, 4, '2010-04-17 14:27:22');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (5, 5, '2010-04-18 11:18:19');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (6, 6, '2010-04-19 13:47:35');
INSERT INTO pedido (CODIGO, ALUNO, DATAHORA) VALUES (7, 6, '2010-04-20 18:13:44');
SELECT * FROM pedido;

-- Comando para adicionar registros na tabela PEDIDO_DETALHE: --
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (1, 1, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (1, 2, 330);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (2, 1, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (2, 2, 330);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (2, 3, 170);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (3, 4, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (4, 2, 330);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (4, 4, 270);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (5, 3, 170);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (6, 3, 170);
INSERT INTO pedido_detalhe (PEDIDO, CURSO, VALOR) VALUES (7, 4, 270);
SELECT * FROM pedido_detalhe;

-- Código do Exercício Proposto 2 da Aula 4:--

-- Exibir todas as informações de todos os alunos: --
SELECT * FROM aluno;

-- Exibir somente o título de cada curso da Softblue: --
SELECT CURSO FROM curso;

-- Exibir somente o título e valor de cada curso da Softblue cujo preço seja maior que 200: -- 
SELECT CURSO, VALOR FROM curso WHERE VALOR > 200;

-- Exibir somente o título e valor de cada curso da Softblue cujo preço seja maior que 200 e menor que 300: --
SELECT CURSO, VALOR FROM curso WHERE VALOR > 200 AND VALOR < 300;
-- Outra solução para o exercício seria esta (usando o comando Between): --
SELECT CURSO, VALOR FROM curso WHERE VALOR BETWEEN 200 AND 300;

-- Exibir as informações da tabela PEDIDOS para os pedidos realizados entre 15/04/2010 e 18/04/2010: --
SELECT DATAHORA FROM pedido WHERE DATAHORA > '2010-04-15' AND DATAHORA < '2010-04-19';
-- Outra solução para o exercício seria esta (usando o comando Between): --
SELECT DATAHORA FROM pedido WHERE DATAHORA BETWEEN '2010-04-15' AND '2010-04-19';

-- Exibir as informações da tabela PEDIDOS para os pedidos realizados na data de 15/04/2010: --
SELECT DATAHORA FROM pedido WHERE DATE(DATAHORA) = '2010-04-15';

-- Código do Exercício Proposto 3 da Aula 4:--

-- Altere o endereço do aluno JOSÉ para 'Av. Brasil 778': --
UPDATE ALUNO SET ENDERECO = 'Av. Brasil 778' WHERE CODIGO = 1;
SELECT * FROM aluno;

-- Altere o e-mail do aluno CRIS para 'cristiano@softblue.com.br': --
UPDATE ALUNO SET EMAIL = 'cristiano@softblue.com.br' WHERE CODIGO = 4;
SELECT * FROM aluno;

-- Aumente em 10% o valor dos cursos abaixo de 300: --
SELECT * FROM curso;
UPDATE CURSO SET VALOR = ROUND(VALOR * 1.1, 1) WHERE VALOR < 300;
SELECT * FROM curso;

-- Altere o nome do curso de Php Básico para Php Fundamentos: --
UPDATE CURSO SET CURSO = 'Php Fundamentos' WHERE CURSO = 'Php Básico';
SELECT * FROM curso;

-- Código do Exercício Proposto 1 da Aula 5:--
USE softblue;
-- Exibe uma lista com os títulos dos cursos da Softblue e o tipo de curso ao lado: --
SELECT curso.CURSO, tipo.TIPO FROM curso INNER JOIN TIPO ON curso.TIPO = tipo.CODIGO;

-- Exiba uma lista com os títulos dos cursos da Softblue, tipo do curso, nome do instrutor responsável pelo mesmo e telefone: --
SELECT curso.CURSO, tipo.TIPO, instrutor.INSTRUTOR, instrutor.TELEFONE FROM CURSO INNER JOIN TIPO ON curso.TIPO = tipo.CODIGO INNER JOIN INSTRUTOR ON curso.INSTRUTOR = instrutor.CODIGO;

-- Exiba uma lista com o código e data e hora dos pedidos e os códigos dos cursos de cada pedido: --
SELECT CODIGO, DATAHORA, CURSO FROM pedido INNER JOIN pedido_detalhe ON pedido.CODIGO = pedido_detalhe.PEDIDO;

-- Exiba uma lista com o código e data e hora dos pedidos e os títulos dos cursos de cada pedido: --
SELECT pedido.CODIGO, DATAHORA, curso.CURSO FROM pedido INNER JOIN pedido_detalhe ON pedido.CODIGO = pedido_detalhe.PEDIDO INNER JOIN curso ON pedido_detalhe.CURSO = curso.CODIGO;

-- Exiba uma lista com o código e data e hora dos pedidos, nome do aluno e os títulos dos cursos de cada pedido: --
SELECT pedido.CODIGO, DATAHORA, aluno.ALUNO, curso.CURSO FROM pedido INNER JOIN pedido_detalhe ON pedido.CODIGO = pedido_detalhe.PEDIDO INNER JOIN aluno ON pedido.ALUNO = aluno.CODIGO INNER JOIN curso ON pedido_detalhe.CURSO = curso.CODIGO;

-- Código do Exercício Proposto 2 da Aula 5:--

-- Crie uma visão que traga o título e preço somente dos cursos de programação da Softblue: --
CREATE VIEW cursos_programacao AS SELECT CURSO, VALOR FROM curso INNER JOIN tipo ON curso.TIPO = tipo.CODIGO WHERE tipo.TIPO = 'Programação';
SELECT * FROM cursos_programacao;

-- Crie uma visão que traga os títulos dos cursos, tipo do curso e nome do instrutor: --
CREATE VIEW cursos_programacao_completo AS SELECT CURSO, tipo.TIPO, instrutor.INSTRUTOR FROM curso INNER JOIN tipo ON curso.TIPO = tipo.CODIGO INNER JOIN instrutor on curso.INSTRUTOR = instrutor.CODIGO;
SELECT * FROM cursos_programacao_completo;

-- Crie uma visão que exiba os pedidos realizados, informando o nome do aluno, data e código do pedido: --
CREATE VIEW pedidos_realizados AS SELECT pedido.CODIGO, aluno.ALUNO, pedido.DATAHORA FROM pedido INNER JOIN aluno ON pedido.ALUNO = aluno.CODIGO;
SELECT * FROM pedidos_realizados;

-- Código do Exercício Proposto 1 da Aula 6:--
USE softblue;

SELECT * FROM aluno;
SELECT * FROM pedido;

-- Selecione os nomes de todos os alunos que já fizeram alguma matrícula na Softblue, sem repetição: --
SELECT DISTINCT(aluno.ALUNO) FROM pedido INNER JOIN aluno ON pedido.ALUNO = aluno.CODIGO;

-- Exiba o nome do aluno mais antigo da Softblue: --
SELECT DISTINCT(aluno.ALUNO) FROM pedido INNER JOIN aluno ON pedido.ALUNO = aluno.CODIGO ORDER BY DATAHORA ASC LIMIT 1;

-- Exiba o nome do aluno mais recente da Softblue: --
SELECT DISTINCT(aluno.ALUNO) FROM pedido INNER JOIN aluno ON pedido.ALUNO = aluno.CODIGO ORDER BY DATAHORA DESC LIMIT 1;

-- Exiba o nome do terceiro aluno mais antigo da Softblue: --
SELECT DISTINCT(aluno.ALUNO) FROM pedido INNER JOIN aluno ON pedido.ALUNO = aluno.CODIGO ORDER BY DATAHORA ASC LIMIT 1 OFFSET 2;

-- Exiba a quantidade de cursos que já foram vendidos pela Softblue: --
SELECT * FROM pedido_detalhe;
SELECT COUNT(*) FROM pedido_detalhe;

-- Exiba o valor total já arrecadado pelos cursos vendidos pela Softblue: --
SELECT SUM(VALOR) FROM pedido_detalhe;

-- Exiba o valor médio cobrado por curso para o pedido cujo CODIGO é 2: --
SELECT AVG(VALOR) FROM pedido_detalhe WHERE PEDIDO = 2;

-- Exiba o valor do curso mais caro da Softblue: --
SELECT VALOR FROM curso;
SELECT MAX(VALOR) FROM curso;

-- Exiba o valor do curso mais barato da Softblue: --
SELECT VALOR FROM curso;
SELECT MIN(VALOR) FROM curso;

-- Exiba o valor total de cada pedido realizado na Softblue: --
SELECT PEDIDO, SUM(VALOR) FROM pedido_detalhe GROUP BY PEDIDO;

-- Exiba os nomes dos instrutores da Softblue e a quantidade de cursos que cada um tem sob sua responsabilidade: --
SELECT instrutor.INSTRUTOR, COUNT(*) FROM curso INNER JOIN INSTRUTOR ON curso.INSTRUTOR = instrutor.CODIGO GROUP BY INSTRUTOR;

-- Exiba o número do pedido, nome do aluno e valor para todos os pedidos realizados na Softblue cujo valor total sejam maiores que 500: --
SELECT PEDIDO, aluno.ALUNO, SUM(VALOR) FROM pedido_detalhe INNER JOIN PEDIDO ON pedido_detalhe.PEDIDO = pedido.CODIGO INNER JOIN ALUNO ON pedido.ALUNO = aluno.CODIGO GROUP BY PEDIDO HAVING SUM(VALOR) > 500;

-- Exiba o número do pedido, nome do aluno e quantos cursos foram comprados no pedido para todos os pedidos realizados na Softblue que compraram dois ou mais cursos: --
SELECT PEDIDO, aluno.ALUNO, COUNT(*) FROM pedido_detalhe INNER JOIN PEDIDO ON pedido_detalhe.PEDIDO = pedido.CODIGO INNER JOIN ALUNO ON pedido.ALUNO = aluno.CODIGO GROUP BY PEDIDO HAVING COUNT(*) > 1;

-- Exiba o nome e endereço de todos os alunos que morem em Avenidas (Av.): --
SELECT ALUNO, ENDERECO FROM aluno WHERE ENDERECO LIKE 'Av%';

-- Exiba os nomes dos cursos de Java da Softblue: --
SELECT CURSO FROM curso WHERE CURSO LIKE '%Java%';

-- Código do Exercício Proposto 2 da Aula 6:--

-- Utilizando subquery, exiba uma lista com os nomes dos cursos disponibilizados pela Softblue informando para cada curso qual o seu menor valor de venda já praticado: --
SELECT CURSO, (SELECT MIN(VALOR) FROM pedido_detalhe WHERE pedido_detalhe.CURSO = curso.CODIGO) AS MENOR_VALOR FROM CURSO;

-- Utilizando subquery e o parâmetro IN, exiba os nomes dos cursos disponibilizados pela Softblue cujo tipo de curso seja 'Programação': --
SELECT * FROM curso;
SELECT * FROM tipo;
SELECT CURSO FROM curso WHERE TIPO IN (SELECT CODIGO FROM tipo WHERE TIPO = 'Programação');

-- Utilizando subquery e o parâmetro EXISTS, exiba novamente os nomes dos cursos disponibilizados pela Softblue cujo tipo de curso seja 'Programação': --
SELECT CURSO FROM curso WHERE EXISTS (SELECT CODIGO FROM tipo WHERE tipo.CODIGO = curso.TIPO AND tipo.TIPO = 'Programação');

-- Exiba uma lista com os nomes dos instrutores da Softblue e ao lado o total acumulado das vendas referente aos cursos pelo qual o instrutor é responsável: --
SELECT INSTRUTOR, (SELECT SUM(pedido_detalhe.VALOR) FROM pedido_detalhe INNER JOIN CURSO ON pedido_detalhe.CURSO = curso.CODIGO WHERE curso.INSTRUTOR = instrutor.CODIGO) AS TOTAL_DE_VENDAS FROM instrutor;

-- Crie uma visão que exiba os nomes dos alunos e quanto cada um já comprou em cursos: --
CREATE VIEW ALUNOS_E_COMPRAS AS SELECT ALUNO, (SELECT SUM(pedido_detalhe.VALOR) FROM pedido_detalhe INNER JOIN PEDIDO ON pedido_detalhe.PEDIDO = pedido.CODIGO WHERE pedido.ALUNO = aluno.CODIGO) AS TOTAL_DE_COMPRAS FROM aluno;
SELECT * FROM ALUNOS_E_COMPRAS;
