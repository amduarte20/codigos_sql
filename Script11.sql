-- Comando para chamar o Banco de Dados: --
USE curso_sql;

-- Código do Exercício Proposto 1 da Aula 7 --

-- Comando para criar uma tabela de Conta Bancária: --
CREATE TABLE CONTA_BANCARIA
(
CODIGO INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
TITULAR VARCHAR(32) NOT NULL,
SALDO DOUBLE NOT NULL,
PRIMARY KEY(CODIGO)
) ENGINE = INNODB;

-- Comando para adicionar alguns registros: --
INSERT INTO CONTA_BANCARIA (CODIGO, TITULAR, SALDO) VALUES (1, 'André', 213);
INSERT INTO CONTA_BANCARIA (CODIGO, TITULAR, SALDO) VALUES (2, 'Diogo', 489);
INSERT INTO CONTA_BANCARIA (CODIGO, TITULAR, SALDO) VALUES (3, 'Rafael', 568);
INSERT INTO CONTA_BANCARIA (CODIGO, TITULAR, SALDO) VALUES (4, 'Carlos', 712);
INSERT INTO CONTA_BANCARIA (CODIGO, TITULAR, SALDO) VALUES (5, 'Peter', -38);
SELECT * FROM CONTA_BANCARIA;

-- Comando para transação de índice de correção percentual de 3% para todas as contas existentes: --
START TRANSACTION;
UPDATE CONTA_BANCARIA SET SALDO = SALDO + (SALDO / 100 * 3) WHERE SALDO > 0;
COMMIT;
SELECT * FROM CONTA_BANCARIA;
