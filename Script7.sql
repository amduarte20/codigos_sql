-- Comando inicial para conectar o Banco de Dados: --
USE curso_sql;

-- Comando para mostrar a tabela de Funcionários: --
SELECT * FROM funcionarios;

-- Comando para fazer a CONTAGEM de funcionários existentes: --
SELECT COUNT(*) FROM funcionarios;

-- Comando para fazer a CONTAGEM de quantos funcionários ganham mais que 1600 reais: --
SELECT COUNT(*) FROM funcionarios WHERE salario > 1600;

-- Comando para fazer a CONTAGEM de quantos e quais são os funcionários que ganham mais que 1600 reais e trabalham no Jurídico: --
SELECT COUNT(*) FROM funcionarios WHERE salario > 1600 AND departamento = 'Jurídico';
SELECT * FROM funcionarios WHERE salario > 1600 AND departamento = 'Jurídico';

-- Comando para saber a SOMA dos salários da Empresa: --
SELECT SUM(salario) FROM funcionarios;

-- Comando para saber a SOMA dos salários só dos trabalhadores de TI: --
SELECT SUM(salario) FROM funcionarios WHERE departamento = 'TI';

-- Comando para saber a MÉDIA dos salários da Empresa: --
SELECT AVG(salario) FROM funcionarios;

-- Comando para saber a MÉDIA dos salários só dos trabalhadores de TI: --
SELECT AVG(salario) FROM funcionarios WHERE departamento = 'TI';

-- Comando para saber o MÁXIMO salário da tabela: --
SELECT MAX(salario) FROM funcionarios;

-- Comando para saber o MÁXIMO salário no departamento de Ti: --
SELECT MAX(salario) FROM funcionarios WHERE departamento = 'TI';

-- Comando para saber o MÍNIMO salário da tabela: --
SELECT MIN(salario) FROM funcionarios;

-- Comando para saber o MÍNIMO salário no departamento de Ti: --
SELECT MIN(salario) FROM funcionarios WHERE departamento = 'TI';

-- Comando para mostrar todos os departamentos da tabela (COM REPETIÇÃO): --
SELECT departamento FROM funcionarios;

-- Comando paRa mostrar todos os departamentos da tabela (SEM REPETIÇÃO): --
SELECT DISTINCT(departamento) FROM funcionarios;

-- Comando para ORDENAÇÃO dos registros da tabela: --
SELECT * FROM funcionarios; -- (Comando Normal) --

SELECT * FROM funcionarios ORDER BY nome; -- (Em ordem crescente de nomes)

SELECT * FROM funcionarios ORDER BY nome ASC; -- (Em ordem crescente de nomes)

SELECT * FROM funcionarios ORDER BY nome DESC; -- (Em ordem decrescente de nomes)

-- Ordenando por salário: --
SELECT * FROM funcionarios ORDER BY salario; -- (Em ordem crescente de salários)

-- Ordenando por departamento: --
SELECT * FROM funcionarios ORDER BY departamento; -- (Em ordem crescente de departamento)

-- Ordenando os salários dentro de cada departamento: --
SELECT * FROM funcionarios ORDER BY departamento, salario; -- (Em ordem crescente de salários por departamento)

-- Ordenando os salários dentro de cada departamento: --
SELECT * FROM funcionarios ORDER BY departamento, salario DESC; -- (Em ordem decrescente de salários por departamento)

-- Ordenando decrescentenmente os salários e departamentos: --
SELECT * FROM funcionarios ORDER BY departamento DESC, salario DESC; -- (Em ordem crescente de salários por departamento)

-- Comandos de PAGINAÇÃO dos registros da tabela: --
SELECT * FROM funcionarios;

-- Comando para limitar a busca para apresentar apenas 2 registros: --
SELECT * FROM funcionarios LIMIT 2;

-- Comando para limitar a busca para apresentar apenas 2 registros e pular o primeiro registro: --
SELECT * FROM funcionarios LIMIT 2 OFFSET 1;
-- ou também podemos fazer assim: --
SELECT * FROM funcionarios LIMIT 1, 2;

-- Comando para AGRUPAMENTO dos registros da tabela: 
-- OBS: Funções de Agrupamento retornam, por vezes, os primeiros registros dependendo do comando desejado. --
-- Comandos Normais: --
SELECT AVG(salario) FROM funcionarios;
SELECT AVG(salario) FROM funcionarios WHERE departamento = 'TI';
SELECT AVG(salario) FROM funcionarios WHERE departamento = 'Jurídico';

-- Comando para calcular a média de cada departamento: --
SELECT departamento, AVG(salario) FROM funcionarios GROUP BY departamento;

-- Comando para calcular a média de cada departamento cuja a média seja acima de 2000 reais: --
SELECT departamento, AVG(salario) FROM funcionarios GROUP BY departamento HAVING AVG(salario) > 2000;

-- Comando para saber quantos funcionarios por departamento a Empresa possui: --
SELECT departamento, COUNT(*) FROM funcionarios GROUP BY departamento;

-- Comandos para construção de SUBQUERIES: --

-- Comando para mostrar os nomes dos funcionários dos departamentos cuja média salarial é maior que 2000 reais: --
SELECT departamento, AVG(salario) FROM funcionarios GROUP BY departamento;
SELECT nome FROM funcionarios WHERE departamento = 'Jurídico';

-- Resolvendo o mesmo caso, porém em uma unica linha: -- (Construção de um Subquery) --
SELECT nome FROM funcionarios WHERE departamento IN (SELECT departamento FROM funcionarios GROUP BY departamento HAVING AVG(salario) > 2000);
